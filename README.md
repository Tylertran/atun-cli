# Atun CLI

CLI for atun-core

The files below are in `tmp` directory.

If there's error
`Could not find browser revision 782078`,
try setting environment variable like this:

```
export ATUN_BROWSER_PATH=node_modules/puppeteer/.local-chromium/mac-818858/chrome-mac/Chromium.app/Contents/MacOS/Chromium
```

On Windows:

```
Powershell:
$env:ATUN_BROWSER_PATH = './node_modules/puppeteer/.local-chromium/win64-818858/chrome-win/chrome.exe'

Cmd:
setx ATUN_BROWSER_PATH './node_modules/puppeteer/.local-chromium/win64-818858/chrome-win/chrome.exe'
```

## Login to your Facebook, store cookies to cookies.json

You need to login (one time) before running later steps:

`npm run login`

## Fetch story links from Facebook, store to story-links.json

`npm run fetch-story-links -- <username>`

## Fetch story bodies from Facebook, store to story-bodies.json

`npm run fetch-story-bodies`

## Generate story-main.pdf

`npm run generate-story-article-pdfs`

## Generate story-toc.pdf

`npm run generate-story-toc-pdf`

## Generate cover.pdf from cover.jpg

`npm run generate-cover-pdf`

postermywall.com is a good site to create book cover online.

## Merge cover.pdf story-toc.pdf, and story-main.pdf to stories.pdf

`npm run merge-story-pdfs`

If there's out of memory problem causing the command above to crash,
alternatively you can use Ghostscript to merge the PDF files:

```
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress \
-sOutputFile=stories.pdf cover.pdf story-toc.pdf story-main.pdf
```

On Mac, you can use Brew to install it:

`brew install ghostscript`

## Commands for Facebook notes

```
npm run login

npm run fetch-note-links -- <username>
npm run fetch-note-bodies

npm run generate-note-article-pdfs
npm run generate-note-toc-pdf

npm run generate-cover-pdf
npm run merge-note-pdfs
```
