import {launchBrowser, newPageMaker, generateTocPdf} from 'atun-core'

import {COOKIES, PDF_FORMAT} from '../config'
import {loadArticles} from '../db'
import {getArgs} from '../util'

const args = getArgs()
if (args.length < 4) {
  console.log('Please specify: links.json bodies.json article1.pdf article2.pdf ... toc.pdf')
  process.exit(1)
}

const linksPath = args[0]
const bodiesPath = args[1]
const articlePdfPaths = args.slice(2, args.length - 1)
const tocPdfPath = args[args.length - 1]

launchBrowser(async (browser) => {
  const articles = loadArticles(linksPath, bodiesPath)
  const pageMaker = newPageMaker(browser)
  await generateTocPdf(articles, pageMaker, PDF_FORMAT, articlePdfPaths, false, tocPdfPath)
}, COOKIES)
