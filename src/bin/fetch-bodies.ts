import {Link, fetchStoryBodies, fetchNoteBodies , launchBrowser, newPageMaker} from 'atun-core'

import {COOKIES,NOTE_LINKS, NOTE_BODIES, STORY_BODIES, STORY_LINKS} from '../config'
import {loadLinks, storeBodies} from '../db'
import {requestCancelOnSigInt} from '../util'

import {LinksType} from './fetch-links'

export function fetchBodies(linksType: LinksType) {
  launchBrowser(async (browser) => {
    const pageMaker = newPageMaker(browser)

    const [linksFile, bodiesFile, fetchFunc] = linksType === LinksType.note
      ? [NOTE_LINKS, NOTE_BODIES, fetchNoteBodies]
      : [STORY_LINKS, STORY_BODIES, fetchStoryBodies]

    const links = loadLinks(linksFile)

    const bodies: string[] = []
    const badLinks: Link[] = []

    for await (const fetchedBody of fetchFunc(pageMaker, links, requestCancelOnSigInt())) {
      const {idx} = fetchedBody
      console.log(`${idx + 1}/${links.length}`)

      const link = links[idx]
      if ('error' in fetchedBody) {
        console.log(`Could not process ${link}`, fetchedBody.error)
        badLinks.push(link)
      } else {
        bodies.push(fetchedBody.body)
      }
    }

    storeBodies(bodiesFile, bodies)

    if (badLinks.length > 0) {
      console.log(`${badLinks.length} bad links:\n` + JSON.stringify(badLinks, null, 2))
    }
  }, COOKIES, {handleSIGINT: false})
}
