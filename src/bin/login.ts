import {DEVICE_IPAD, launchBrowser, newFbPage, newPageMaker, storeCookies} from 'atun-core'
import {COOKIES} from '../config'

launchBrowser(async (browser) => {
  const page = await newFbPage(newPageMaker(browser), DEVICE_IPAD, false, '')
  await page.waitForSelector('#MComposer', {timeout: 0})
  await storeCookies(COOKIES, page)
  process.exit(0)
}, COOKIES, {headless: false})
