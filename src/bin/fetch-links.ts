import {fetchNoteLinks, fetchStoryLinks, launchBrowser, newPageMaker} from 'atun-core'

import {COOKIES, NOTE_LINKS, STORY_LINKS} from '../config'
import {storeLinks} from '../db'
import {getArgs, requestCancelOnSigInt} from '../util'

const storyLinksTitleOptions = {
  maxChars: 128,
  upperCaseRatio: 0.3
}

export enum LinksType {
  note, story
}

export function fetchLinks(linksType: LinksType) {
  const args = getArgs()
  if (args.length != 1) {
    console.log('Please specify username: npm run links <target username>')
    process.exit(1)
  }

  const username = args[0]

  const [path, linksFile, fetchFunc, titleOptions] = linksType === LinksType.note
    ? [`/${username}/notes`, NOTE_LINKS, fetchNoteLinks, undefined]
    : [`/${username}`, STORY_LINKS, fetchStoryLinks, storyLinksTitleOptions]

  launchBrowser(async (browser) => {
    const links = []

    for await (const newLinks of fetchFunc(newPageMaker(browser), path, requestCancelOnSigInt(), titleOptions)) {
      links.push(...newLinks)
      console.log(`Got ${newLinks.length} more links, ${links.length} in total`)
    }

    storeLinks(linksFile, links)
  }, COOKIES, {handleSIGINT: false})
}
