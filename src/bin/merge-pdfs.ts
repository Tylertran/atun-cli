import {mergePdfs} from 'atun-core'
import {getArgs} from '../util'

const pdfs = getArgs()
if (pdfs.length < 3) {
  console.log('Please specify PDFs: input1.pdf input2.pdf ... merged.pdf')
  process.exit(1)
}

const main = async () => {
  const mergedPdf = pdfs.pop()!
  await mergePdfs(pdfs, mergedPdf)
  console.log(`Merged to ${mergedPdf}`)
}
main()
