import {launchBrowser, newPageMaker, generateArticlePdfs} from 'atun-core'

import {COOKIES, PDF_FORMAT} from '../config'
import {loadArticles} from '../db'
import {getArgs} from '../util'

const paths = getArgs()
if (paths.length !== 3) {
  console.log('Please specify links.json and bodies.json')
  process.exit(1)
}

const linksPath = paths[0]
const bodiesPath = paths[1]
const articlePdfPathPrefix = paths[2]

launchBrowser(async (browser) => {
  const articles = loadArticles(linksPath, bodiesPath)
  const pageMaker = newPageMaker(browser)
  await generateArticlePdfs(articles, pageMaker, PDF_FORMAT, (articleIdx) =>
    Promise.resolve(`${articlePdfPathPrefix}-${articleIdx.toString().padStart(3, '0')}.pdf`)
  )
}, COOKIES)
