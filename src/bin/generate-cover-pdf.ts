import {generateCoverPdf, getImageDataUrl, launchBrowser, newPageMaker} from 'atun-core'
import {COOKIES, COVER_IMAGE, COVER_PDF, PDF_FORMAT} from '../config'

launchBrowser(async (browser) => {
  const dataUrl = getImageDataUrl(COVER_IMAGE)
  const pageMaker = newPageMaker(browser)
  await generateCoverPdf(dataUrl, pageMaker, PDF_FORMAT, COVER_PDF)
}, COOKIES)
