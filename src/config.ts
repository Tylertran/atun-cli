export const PDF_FORMAT = 'A4'
export const COOKIES = 'tmp/cookies.json'

export const STORY_LINKS = 'tmp/story-links.json'
export const STORY_BODIES = 'tmp/story-bodies.json'

export const NOTE_LINKS = 'tmp/note-links.json'
export const NOTE_BODIES = 'tmp/note-bodies.json'

export const COVER_IMAGE = 'tmp/cover.jpg'
export const COVER_PDF = 'tmp/cover.pdf'
