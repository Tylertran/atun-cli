// Assume that the program is run with:
// node program.js arg1 arg2 arg3...
export function getArgs() {
  // Copy
  const args = [...process.argv]

  // Remove 2 first args (paths to node and the currently run script)
  args.shift()
  args.shift()

  return args
}

export function requestCancelOnSigInt() {
  console.log('Ctrl+C to cancel')
  let cancelRequested = false

  process.on('SIGINT', function() {
    console.log('Canceled')
    cancelRequested = true
  })

  return () => cancelRequested
}
