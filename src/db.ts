import {Link, loadJson, storeJson} from 'atun-core'

export function storeLinks(linksPath: string, links: Link[]) {
  storeJson(linksPath, links)
  console.log(`${links.length} links stored to ${linksPath}`)
}

export function loadLinks(linksPath: string): Link[] {
  const links = loadJson(linksPath)
  console.log(`${links.length} links loaded from ${linksPath}`)
  return links
}

export function storeBodies(bodiesPath: string, bodies: string[]) {
  storeJson(bodiesPath, bodies)
  console.log(`${bodies.length} stored to ${bodiesPath}`)
}

export function loadBodies(bodiesPath: string): string[] {
  const bodies = loadJson(bodiesPath)
  console.log(`${bodies.length} bodies loaded from ${bodiesPath}`)
  return bodies
}

// bodies.length can be < links.length,
// if there was cancelation when getting bodies.
export function loadLinksAndBodies(linksPath: string, bodiesPath: string): [Link[], string[]] {
  const links = loadLinks(linksPath)
  const bodies = loadBodies(bodiesPath)

  const numStories = Math.min(links.length, bodies.length)
  links.splice(numStories - 1)
  bodies.splice(numStories - 1)

  return [links, bodies]
}

export function loadArticles(linksPath: string, bodiesPath: string) {
  const [links, bodies] = loadLinksAndBodies(linksPath, bodiesPath)

  const articles = bodies.map((body, idx) => ({
    title: links[idx].title,
    date: links[idx].date,
    body
  }))

  return articles
}
